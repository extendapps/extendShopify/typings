import {Receipt} from './Receipt';
import {PaymentDetails} from './PaymentDetails';

export interface Transaction {
    id: number;
    order_id: number;
    amount: string;
    kind: 'authorization' | 'capture' | 'sale' | 'void' | 'refund';
    gateway: string;
    status: 'pending' | 'failure' | 'success' | 'error';
    message?: string;
    created_at: string;
    test: boolean;
    authorization: string;
    currency: string;
    location_id?: any;
    user_id?: any;
    parent_id?: any;
    device_id?: number;
    receipt: Receipt;
    error_code?: 'incorrect_number' | 'invalid_number' | 'invalid_expiry_date' | 'invalid_cvc' | 'expired_card' | 'incorrect_cvc' | 'incorrect_zip' | 'incorrect_address' | 'card_declined' | 'processing_error' | 'call_issuer' | 'pick_up_card';
    source_name: string;
    admin_graphql_api_id: string;
    payment_details: PaymentDetails;
}

import {WebHookTopic} from '../Interfaces/ShopifyTypes';

export interface WebHook {
    address: string;
    created_at?: string;
    fields?: string[];
    format: 'json' | 'xml';
    id?: number;
    metafield_namespaces?: string[];
    topic: WebHookTopic;
    updated_at?: string;
}

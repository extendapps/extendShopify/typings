export interface DiscountAllocation {
    amount: string;
    discount_application_index: number;
}

import {Address} from './Address';
import {CreateCustomerAddressContextFunction, GetContextCustomerAddressesFunction, GetCustomerAddressFunctionContext, UpdateCustomerAddressContextFunction} from '../Interfaces/ICustomerAddress';

export interface Customer {
    id: number;
    email: string;
    accepts_marketing: boolean;
    created_at?: string;
    updated_at?: string;
    first_name: string;
    last_name: string;
    orders_count: number;
    state: string;
    total_spent: string;
    last_order_id?: any;
    note?: any;
    verified_email: boolean;
    multipass_identifier?: any;
    tax_exempt: boolean;
    phone?: string;
    tags: string;
    last_order_name?: any;
    default_address: Address;
    getAddresses: GetContextCustomerAddressesFunction;
    getAddress: GetCustomerAddressFunctionContext;
    createAddress: CreateCustomerAddressContextFunction;
    updateAddress: UpdateCustomerAddressContextFunction;
}

import {OrderAdjustment} from './OrderAdjustment';
import {RefundLineItem} from './RefundLineItem';
import {Transaction} from './Transaction';
import {Base} from './Base';

export interface Refund extends Base {
    order_id: number;
    note: string;
    user_id: number;
    processed_at: string;
    restock: boolean;
    admin_graphql_api_id: string;
    refund_line_items: RefundLineItem[];
    transactions: Transaction[];
    order_adjustments: OrderAdjustment[];
}

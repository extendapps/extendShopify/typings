export interface Address {
    id?: number;
    customer_id: number;
    first_name?: any;
    last_name?: any;
    company?: any;
    address1: string;
    address2?: any;
    city: string;
    province: string;
    country: string;
    zip: string;
    phone: string;
    name: string;
    province_code: string;
    country_code: string;
    country_name: string;
    default: boolean;
}

export interface LatLongAddress extends Address {
    latitude?: any;
    longitude?: any;
}

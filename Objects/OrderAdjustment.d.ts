export interface OrderAdjustment {
    id: number;
    order_id: number;
    refund_id: number;
    amount: string;
    tax_amount: string;
    kind: 'shipping_refund' | 'refund_discrepancy';
    reason: string;
}

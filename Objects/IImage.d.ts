export interface IImage {
    id: number;
    product_id: number;
    position: number;
    created_at: string;
    updated_at: string;
    alt?: any;
    width: number;
    height: number;
    src: string;
    variant_ids: number[];
    admin_graphql_api_id: string;
}

import {TaxLine} from './TaxLine';

export interface ShippingLine {
    id: number;
    title: string;
    price: string;
    code?: any;
    source: string;
    phone?: any;
    requested_fulfillment_service_id?: any;
    delivery_category?: any;
    carrier_identifier?: any;
    discounted_price: string;
    discount_allocations: any[];
    tax_lines: TaxLine[];
}

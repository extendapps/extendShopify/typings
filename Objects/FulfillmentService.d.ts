export interface FulfillmentService {
    id: number;
    name: string;
    handle: string;
    email?: any;
    include_pending_stock: boolean;
    requires_shipping_method: boolean;
    service_name: string;
    inventory_management: boolean;
    tracking_support: boolean;
    provider_id?: any;
    location_id: number;
    format: 'json' | 'xml';
    callback_url?: string;
}

import {IOption} from './IOption';
import {IImage} from './IImage';
import {GetContextProductVariantsCountFunction, GetContextProductVariantsFunction} from '../Interfaces/IProductVariants';
import {GetContextProductImagesFunction} from '../Interfaces/IProductImages';
import {Variant} from './Variant';

export interface Product {
    id?: number;
    title: string;
    body_html: string;
    vendor: string;
    product_type: string;
    created_at?: string;
    handle?: string;
    updated_at?: string;
    published_at?: string;
    template_suffix?: any;
    tags?: string;
    published_scope?: string;
    admin_graphql_api_id?: string;
    variants?: Variant[];
    options?: IOption[];
    images?: IImage[];
    image?: IImage;
    published?: boolean;
    getVariants?: GetContextProductVariantsFunction;
    getVariantsCount?: GetContextProductVariantsCountFunction;
    getImages?: GetContextProductImagesFunction;
}

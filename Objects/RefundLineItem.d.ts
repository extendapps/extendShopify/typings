import {LineItem} from './LineItem';

export interface RefundLineItem {
    id: number;
    quantity: number;
    line_item_id: number;
    location_id: number;
    restock_type: 'no_restock' | 'cancel' | 'return' | 'legacy_restock';
    subtotal: number;
    total_tax: number;
    line_item: LineItem;
}

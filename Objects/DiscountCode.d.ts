export interface DiscountCode {
    amount: string;
    code: string;
    type: 'fixed_amount' | 'percentage' | 'shipping';
}

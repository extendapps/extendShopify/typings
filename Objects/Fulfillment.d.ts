export interface Fulfillment {
    created_at: string;
    updated_at: string;
    id: number;
    order_id: number;
    status: string;
    tracking_company: string;
    tracking_number: string;
}

export interface TaxLine {
    title: string;
    price: string;
    rate: number;
}

import {FulfillmentStatus} from '../Interfaces/ShopifyTypes';
import {DiscountAllocation} from './DiscountAllocation';
import {Property} from './Property';
import {TaxLine} from './TaxLine';

export interface LineItem {
    id: any;
    variant_id?: any;
    title: string;
    quantity: number;
    price: string;
    sku: string;
    variant_title?: any;
    vendor?: any;
    fulfillment_service: string;
    product_id: number;
    requires_shipping: boolean;
    taxable: boolean;
    gift_card: boolean;
    name: string;
    variant_inventory_management?: any;
    properties: Property[];
    product_exists: boolean;
    fulfillable_quantity: number;
    grams: number;
    pre_tax_price: string;
    total_discount: string;
    fulfillment_status?: FulfillmentStatus;
    discount_allocations: DiscountAllocation[];
    tax_lines: TaxLine[];
    netSuiteId: number;
}

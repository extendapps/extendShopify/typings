import {GetOrderOptions, GetOrdersCountOptions, GetOrdersOptions} from './Interfaces/IOrder';
import {CalculateOrderRefundOptions, GetOrderRefundOptions, GetOrderRefundsOptions} from './Interfaces/IOrderRefund';
import {CreateOrderTransactionOptions, GetOrderTransactionCountOptions, GetOrderTransactionOptions, GetOrderTransactionsOptions} from './Interfaces/IOrderTransaction';
import {CreateProductOptions, DeleteProductOptions, GetProductOptions, GetProductsOptions, UpdateProductOptions} from './Interfaces/IProduct';
import {GetProductImagesOptions} from './Interfaces/IProductImages';
import {CreateProductVariantOptions, DeleteProductVariantOptions, GetProductVariantOptions, GetProductVariantsCountOptions, GetProductVariantsOptions, UpdateProductVariantOptions} from './Interfaces/IProductVariants';
import {GetShopPropertiesOptions} from './Interfaces/IShop';
import {CreateWebHookOptions, DeleteWebHookOptions, GetWebHookOptions, GetWebHooksOptions, UpdateWebHookOptions, ValidateWebHookOptions} from './Interfaces/IWebHook';
import {CreateCustomerAddressOptions, GetCustomerAddressesOptions, GetCustomerAddressOptions, UpdateCustomerAddressOptions} from './Interfaces/ICustomerAddress';
import {CreateFulfillmentServiceOptions, DeleteFulfillmentServiceOptions, GetFulfillmentServiceOptions, GetFulfillmentServicesOptions, UpdateFulfillmentServiceOptions} from './Interfaces/IFulfillmentService';
import {GetNetSuiteItemIdsOptions} from './Interfaces/ShopifyTypes';

export interface SHOPIFY_API {
    getWebHookAddress(): string;

    getOrders(options: GetOrdersOptions): void;

    getOrdersCount(options: GetOrdersCountOptions): void;

    getOrder(options: GetOrderOptions): void;

    getOrderRefunds(options: GetOrderRefundsOptions): void;

    getOrderRefund(options: GetOrderRefundOptions): void;

    calculateOrderRefund(options: CalculateOrderRefundOptions): void;

    getOrderTransactions(options: GetOrderTransactionsOptions): void;

    getOrderTransactionCount(options: GetOrderTransactionCountOptions): void;

    getOrderTransaction(options: GetOrderTransactionOptions): void;

    createOrderTransaction(options: CreateOrderTransactionOptions): void;

    updateProduct(options: UpdateProductOptions): void;

    createProduct(options: CreateProductOptions): void;

    getProducts(options: GetProductsOptions): void;

    getProduct(options: GetProductOptions): void;

    deleteProduct(options: DeleteProductOptions): void;

    getProductImages(options: GetProductImagesOptions): void;

    getProductVariants(options: GetProductVariantsOptions): void;

    getProductVariantsCount(options: GetProductVariantsCountOptions): void;

    getProductVariant(options: GetProductVariantOptions): void;

    updateProductVariant(options: UpdateProductVariantOptions): void;

    createProductVariant(options: CreateProductVariantOptions): void;

    deleteProductVariant(options: DeleteProductVariantOptions): void;

    getShopProperties(options: GetShopPropertiesOptions): void;

    validateWebHook(options: ValidateWebHookOptions): void;

    getWebHook(options: GetWebHookOptions): void;

    getWebHooks(options: GetWebHooksOptions): void;

    createWebHook(options: CreateWebHookOptions): void;

    updateWebHook(options: UpdateWebHookOptions): void;

    deleteWebHook(options: DeleteWebHookOptions): void;

    getCustomerAddresses(options: GetCustomerAddressesOptions): void;

    getCustomerAddress(options: GetCustomerAddressOptions): void;

    createCustomerAddress(options: CreateCustomerAddressOptions): void;

    updateCustomerAddress(options: UpdateCustomerAddressOptions): void;

    getFulfillmentServices(options: GetFulfillmentServicesOptions): void;

    createFulfillmentService(options: CreateFulfillmentServiceOptions): void;

    getFulfillmentService(options: GetFulfillmentServiceOptions): void;

    updateFulfillmentService(options: UpdateFulfillmentServiceOptions): void;

    deleteFulfillmentService(options: DeleteFulfillmentServiceOptions): void;

    getNetSuiteItemIds(options: GetNetSuiteItemIdsOptions): void;
}

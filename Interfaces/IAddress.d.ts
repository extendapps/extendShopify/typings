import {Address} from '../Objects/Address';

export declare type AddressCallBack = (address: Address) => void;
export declare type AddressesCallBack = (addresses: Address[]) => void;
export declare type CustomerAddressCallBack = (customer_address: Address) => void;

export interface AddressPayload {
    address: Address;
}

export interface AddressesPayload {
    addresses: Address[];
}

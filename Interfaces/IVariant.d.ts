import {Variant} from '../Objects/Variant';

export declare type VariantCallBack = (variant: Variant) => void;
export declare type VariantsCallBack = (variants: Variant[]) => void;
export declare type VariantsCountCallBack = (count: number) => void;

export interface VariantsPayload {
    variants: Variant[];
}

export interface VariantsCountPayload {
    count: number;
}

export interface VariantPayload {
    variant: Variant;
}

import * as https from 'N/https';

export interface BaseOptions {
    fail?: FailedCallback;
}

export declare type FailedCallback = (clientResponse: https.ClientResponse) => void;

export interface BaseSearchOptions {
    ids?: number[];
    limit?: number;
    since_id?: number;
    created_at_min?: string;
    created_at_max?: string;
    updated_at_min?: string;
    updated_at_max?: string;
    fields?: string[];
}

import {AddressCallBack, AddressesCallBack, AddressPayload, CustomerAddressCallBack} from './IAddress';
import {Address} from '../Objects/Address';
import {BaseOptions} from './Base';
import {CustomerBaseOptions} from './ICustomer';

export interface GetCustomerAddressContextOptions {
    addressId: number;
    OK: AddressCallBack;
}

export interface GetCustomerAddressOptions extends GetCustomerAddressContextOptions, CustomerBaseOptions {
}

export declare type GetCustomerAddressFunctionContext = (options: GetCustomerAddressContextOptions) => void;
export declare type GetContextCustomerAddressesFunction = (options: GetCustomerAddressesOptions) => void;

export interface GetCustomerAddressesOptions extends CustomerBaseOptions {
    OK: AddressesCallBack;
}

export interface UpdateCustomerAddressContextOptions extends BaseOptions, AddressPayload {
    OK: CustomerAddressCallBack;
}

export interface CreateCustomerAddressContextOptions extends BaseOptions, AddressPayload {
    Created: CustomerAddressCallBack;
}

export interface CreateCustomerAddressOptions extends CustomerBaseOptions, CreateCustomerAddressContextOptions {
}

export interface UpdateCustomerAddressOptions extends CustomerBaseOptions, UpdateCustomerAddressContextOptions {
}

export declare type CreateCustomerAddressContextFunction = (options: CreateCustomerAddressContextOptions) => void;
export declare type UpdateCustomerAddressContextFunction = (options: UpdateCustomerAddressContextOptions) => void;

export interface CustomerAddressPayload {
    customer_address: Address;
}

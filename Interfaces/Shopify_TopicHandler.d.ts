import {Customer} from '../Objects/Customer';
import {Order} from '../Objects/Order';
import {Product} from '../Objects/Product';
import {Refund} from '../Objects/Refund';
import {ShopifyEventDetails} from './Shopify_EventDetails';

export declare namespace ShopifyTopicHandler {
    namespace Customer {
        type handler = (eventDetails: ShopifyEventDetails, customer: Customer) => void;
    }
    namespace Order {
        type handler = (eventDetails: ShopifyEventDetails, order: Order) => void;
    }
    namespace Product {
        type handler = (eventDetails: ShopifyEventDetails, product: Product) => void;
    }
    namespace Refund {
        type handler = (eventDetails: ShopifyEventDetails, refund: Refund) => void;
    }
}

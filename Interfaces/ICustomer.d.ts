import {BaseOptions} from './Base';

export interface CustomerBaseOptions extends BaseOptions {
    customerId: number;
}

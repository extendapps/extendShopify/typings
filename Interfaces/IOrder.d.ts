import {FinancialStatus, FulfillmentStatus} from './ShopifyTypes';
import {BaseOptions, BaseSearchOptions} from './Base';
import {Order} from '../Objects/Order';

export interface OrderBaseOptions extends BaseOptions {
    id: number;
}

export interface OrdersBaseOptions extends BaseOptions {
}

export interface GetOrdersBaseOptions extends OrdersBaseOptions, BaseSearchOptions {
    order?: string;
    processed_at_min?: string;
    processed_at_max?: string;
    attribution_app_id?: string;
    status?: 'open' | 'closed' | 'cancelled' | 'any';
    financial_status?: FinancialStatus;
    fulfillment_status?: FulfillmentStatus;
}

export interface GetOrderOptions extends OrderBaseOptions, BaseSearchOptions {
    OK: GetOrderCallback;
}

export interface GetOrdersOptions extends GetOrdersBaseOptions {
    OK: GetOrdersCallback;
}

export interface GetOrdersCountOptions extends GetOrdersBaseOptions {
    OK: GetOrdersCountCallback;
}

export declare type GetOrderCallback = (order: Order) => void;

export interface GetOrderResponse {
    order: Order;
}

export declare type GetOrdersCallback = (orders: Order[]) => void;

export interface GetOrdersResponse {
    orders: Order[];
}

export declare type GetOrdersCountCallback = (count: number) => void;

export interface GetOrdersCountResponse {
    count: number;
}

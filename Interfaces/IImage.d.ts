import {IImage} from '../Objects/IImage';

export declare type IImagesCallback = (images: IImage[]) => void;

export interface IImagesPayload {
    images: IImage[];
}

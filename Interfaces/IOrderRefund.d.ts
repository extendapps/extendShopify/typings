import {RefundCallBack, RefundsCallBack} from './IRefund';
import {OrderBaseOptions} from './IOrder';
import {RefundLineItem} from '../Objects/RefundLineItem';

export interface OrderRefundOptions extends OrderBaseOptions {
    refundId: number;
}

export declare type GetContextOrderRefundFunction = (options: GetContextOrderRefund) => void;

export interface GetContextOrderRefund extends OrderBaseOptions {
    OK: RefundCallBack;
}

export interface GetOrderRefundOptions extends GetContextOrderRefund, OrderRefundOptions {
}

export declare type GetContextOrderRefundsFunction = (options: GetOrderRefundsOptions) => void;

export interface GetContextOrderRefundsOptions extends OrderBaseOptions {
    since_id?: number;
    fields?: string;
    OK: RefundsCallBack;
}

export interface GetOrderRefundsOptions extends GetContextOrderRefundsOptions {
}

export interface CalculateOrderRefundOptions extends OrderBaseOptions {
    refund: {
        shipping: {
            full_refund?: boolean;
            amount?: number;
        };
        refund_line_items?: RefundLineItem[];
    };
    OK: RefundCallBack;
}

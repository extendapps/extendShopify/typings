import {Transaction} from '../Objects/Transaction';

export declare type TransactionCallBack = (transaction: Transaction) => void;
export declare type TransactionsCallBack = (transactions: Transaction[]) => void;
export declare type TransactionDeleteCallBack = () => void;

export interface TransactionPayload {
    transaction: Transaction;
}

export interface TransactionsPayload {
    transactions: Transaction[];
}

export interface TransactionCountPayload {
    count: number;
}

import {VariantCallBack, VariantPayload, VariantsCallBack, VariantsCountCallBack} from './IVariant';
import {BaseOptions} from './Base';

export interface GetContextProductVariantsOptions extends BaseOptions {
    OK: VariantsCallBack;
}

export interface GetContextProductVariantsCountOptions extends BaseOptions {
    OK: VariantsCountCallBack;
}

export interface CreateProductVariantOptions extends BaseOptions, VariantPayload {
    Created: VariantCallBack;
}

export interface UpdateProductVariantOptions extends BaseOptions, VariantPayload {
    OK: VariantCallBack;
}

export declare type GetContextProductVariantsFunction = (options: GetContextProductVariantsOptions) => void;
export declare type GetContextProductVariantsCountFunction = (options: GetContextProductVariantsCountOptions) => void;

export interface GetProductVariantOptions extends BaseOptions {
    variant_id: number;
    OK: VariantCallBack;
}

export interface GetProductVariantsOptions extends GetContextProductVariantsOptions {
    product_id: number;
}

export interface GetProductVariantsCountOptions extends GetContextProductVariantsCountOptions {
    product_id: number;
}

export interface DeleteProductVariantOptions extends BaseOptions {
    product_id: number;
    variant_id: number;
    OK: () => void;
}

import {BaseOptions} from './Base';
import {Shop} from '../Objects/Shop';

export declare type GetShopPropertiesCallback = (shop: Shop) => void;

export interface ShopPropertiesPayload {
    shop: Shop;
}

export interface GetShopPropertiesOptions extends BaseOptions {
    OK: GetShopPropertiesCallback;
}

import {BaseOptions} from './Base';
import {FulfillmentService} from '../Objects/FulfillmentService';

export declare type FulfillmentServiceCallBack = (fulfillment_service: FulfillmentService) => void;
export declare type FulfillmentServicesCallBack = (fulfillment_services: FulfillmentService[]) => void;
export declare type FulfillmentServicesDeleteCallBack = () => void;

export interface FulfillmentServicePayload {
    fulfillment_service: FulfillmentService;
}

export interface FulfillmentServicesPayload {
    fulfillment_services: FulfillmentService[];
}

export interface FulfillmentServiceBaseOptions extends BaseOptions {
    id: number;
}

export interface GetFulfillmentServiceContextOptions {
    OK: FulfillmentServiceCallBack;
}

export interface GetFulfillmentServicesContextOptions {
    OK: FulfillmentServicesCallBack;
}

export interface GetFulfillmentServiceOptions extends GetFulfillmentServiceContextOptions, FulfillmentServiceBaseOptions {
}

export interface GetFulfillmentServicesOptions extends GetFulfillmentServicesContextOptions, FulfillmentServiceBaseOptions {
}

export interface UpdateFulfillmentServiceContextOptions extends BaseOptions, FulfillmentServicePayload {
    OK: FulfillmentServiceCallBack;
}

export interface CreateFulfillmentServiceContextOptions extends BaseOptions, FulfillmentServicePayload {
    Created: FulfillmentServiceCallBack;
}

export interface DeleteFulfillmentServiceOptions extends FulfillmentServiceBaseOptions {
    OK: FulfillmentServicesDeleteCallBack;
}

export interface CreateFulfillmentServiceOptions extends BaseOptions, CreateFulfillmentServiceContextOptions {
}

export interface UpdateFulfillmentServiceOptions extends UpdateFulfillmentServiceContextOptions {
}

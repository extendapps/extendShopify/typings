import {Refund} from '../Objects/Refund';

export declare type RefundCallBack = (refund: Refund) => void;
export declare type RefundsCallBack = (refunds: Refund[]) => void;

export interface RefundPayload {
    refund: Refund;
}

export interface RefundsPayload {
    refunds: Refund[];
}

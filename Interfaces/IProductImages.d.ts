import {BaseOptions} from './Base';
import {IImagesCallback} from './IImage';

export interface GetContextProductImagesOptions extends BaseOptions {
    OK: IImagesCallback;
}

export interface GetProductImagesOptions extends GetContextProductImagesOptions {
    product_id: number;
}

export declare type GetContextProductImagesFunction = (options: GetContextProductImagesOptions) => void;

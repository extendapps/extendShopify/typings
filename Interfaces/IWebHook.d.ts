import {BaseOptions} from './Base';
import {WebHook} from '../Objects/WebHook';

export interface WebHookPayload {
    webhook: WebHook;
}

export declare type WebHookCallback = (webhook: WebHook) => void;

export interface WebHooksPayload {
    webhooks: WebHook[];
}

export declare type WebHooksCallback = (webhooks: WebHook[]) => void;

export interface GetWebHookOptions extends BaseOptions {
    webhookId: number;
    OK: WebHookCallback;
}

export interface GetWebHooksOptions extends BaseOptions {
    OK: WebHooksCallback;
}

export interface UpdateWebHookOptions extends BaseOptions, WebHookPayload {
    OK: WebHookCallback;
}

export interface ValidateWebHookOptions {
    hmacHeader: string;
    webHookData: string;
    success: () => void;
    fail: () => void;
}

export interface CreateWebHookOptions extends BaseOptions, WebHookPayload {
    Created: WebHookCallback;
}

export interface DeleteWebHookOptions extends BaseOptions {
    webhook: WebHook;
    OK: () => void;
}

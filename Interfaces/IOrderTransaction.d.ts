import {TransactionCallBack, TransactionDeleteCallBack, TransactionPayload, TransactionsCallBack} from './ITransaction';
import {BaseOptions} from './Base';
import {OrderBaseOptions} from './IOrder';
import {Transaction} from '../Objects/Transaction';

export interface OrderTransactionBaseOptions extends OrderBaseOptions {
    transactionId: number;
}

export interface GetOrderTransactionContextOptions {
    OK: TransactionCallBack;
}

export interface GetOrderTransactionsContextOptions {
    OK: TransactionsCallBack;
}

export declare type GetContextOrderTransactionFunction = (options: GetOrderTransactionOptions) => void;

export interface GetOrderTransactionOptions extends GetOrderTransactionContextOptions, OrderTransactionBaseOptions {
}

export interface GetOrderTransactionsOptions extends GetOrderTransactionsContextOptions, OrderTransactionBaseOptions {
}

export interface UpdateOrderTransactionContextOptions extends GetOrderTransactionContextOptions, OrderTransactionBaseOptions, TransactionPayload {
}

export declare type CreateContextOrderTransactionFunction = (options: CreateOrderTransactionOptions) => void;

export interface CreateOrderTransactionContextOptions extends OrderTransactionBaseOptions, TransactionPayload {
    Created: TransactionCallBack;
}

export interface DeleteOrderTransactionOptions extends OrderTransactionBaseOptions {
    OK: TransactionDeleteCallBack;
}

export interface CreateOrderTransactionOptions extends BaseOptions, CreateOrderTransactionContextOptions {
}

export interface UpdateOrderTransactionOptions extends OrderTransactionBaseOptions, UpdateOrderTransactionContextOptions {
    id: number;
}

export declare type GetContextOrderTransactionsFunction = (options: GetContextOrderTransactionsOptions) => void;
export declare type GetOrderTransactions = (transactions: Transaction[]) => void;

export interface GetContextOrderTransactionsOptions extends BaseOptions {
    since_id?: number;
    fields?: string;
    OK: GetOrderTransactions;
}

export interface GetOrderTransactionsOptions extends GetContextOrderTransactionsOptions, OrderBaseOptions {
}

export declare type GetContextOrderTransactionCountFunction = (options: GetOrderTransactionCountOptions) => void;
export declare type GetOrderTransactionCount = (count: number) => void;

export interface GetOrderTransactionCountOptions extends OrderBaseOptions {
    OK: GetOrderTransactionCount;
}
